import React, { Component } from 'react';
import { connect } from 'react-redux';

import Cell from './Cell/';
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="line">
          <Cell value={ 5 } />
          <Cell value={ 44 } />
          <Cell value={ 5 } />
          <Cell />
        </div>
        <div className="line">
          <Cell value={ 15 } />
          <Cell value={ 5 } />
          <Cell value={ 5 } />
          <Cell value={ 5 } />
        </div>
        <div className="line">
          <Cell value={ 5 } />
          <Cell value={ 5 } />
          <Cell value={ 5 } />
          <Cell value={ 5 } />
        </div>
        <div className="line">
          <Cell value={ 5 } />
          <Cell value={ 5 } />
          <Cell value={ 5 } />
          <Cell value={ 5 } />
        </div>
        <div>steps taken: </div>
      </div>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    onMoveCell: (cellIndex, cellValue) => {
      dispatch({ type: 'MOVE_CELL', payload: })
    }
  })
)(App);
