import React from 'react';
import './styles.css';

const Cell = (props) => {
  const { value } = props;
  return (
    <div className={ value && "cellWrapper" }>
      <div className={ value && "withValue" }>
        { value }
      </div>
    </div>
  );
}

export default Cell;
