export default function reduce(state, action = {}) {
  switch (action.type) {
    case 'MOVE_CELL':
      return state;
    default:
      return [
        ...state,
        action.payload
      ];
  }
}
